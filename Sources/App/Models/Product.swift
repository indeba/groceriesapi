//
//  Product.swift
//  App
//
//  Created by Hernan G. Gonzalez on 06/10/2018.
//

import FluentSQLite
import Vapor

final class Product: SQLiteUUIDModel {
    var id: UUID?
    var name: String
    
    init(id: UUID? = nil, name: String) {
        self.id = id
        self.name = name
    }
}

extension Product: Content { }
extension Product: Migration { }

