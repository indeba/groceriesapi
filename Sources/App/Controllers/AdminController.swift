//
//  AdminController.swift
//  App
//
//  Created by Hernan G. Gonzalez on 06/10/2018.
//

import Vapor

struct TableRow: Decodable {
    let name: String
    let comment: String
    let ah: String
    let jumbo: String
    let lidl: String
    let zeeman: String
    let kruidvat: String
    let action: String
    let kema: String
    
    init(strings: [String]) {
        var strings = strings
        kema = strings.popLast()!
        action = strings.popLast()!
        kruidvat = strings.popLast()!
        zeeman = strings.popLast()!
        lidl = strings.popLast()!
        jumbo = strings.popLast()!
        ah = strings.popLast()!
        comment = strings.popLast()!
        name = strings.popLast()!
    }
}

struct LoadContent: Decodable {
    let table: [[String]]
}

final class AdminController {
    
    /// Saves a decoded `Todo` to the database.
    func load(_ req: Request) throws -> Future<HTTPStatus> {
        let content = req.content
        let body = try content.decode(LoadContent.self)
        let rows = body.map { $0.table.map { TableRow(strings: $0) } }
        let products = rows.map { rows -> [Future<Product>] in
            let maps = rows.map { Product.create(with: $0, on: req) }
            return maps
        }
        
        let promise = req.eventLoop.newPromise(HTTPStatus.self)
        products.addAwaiter { result in
            switch result {
            case .success(let created):
                let reduced = Future<[Product]>.reduce(Array<Product>.init(), created, eventLoop: body.eventLoop) { (container, prod) -> [Product] in
                    return container
                }
                
                reduced.addAwaiter(callback: { result in
                    switch result {
                    case .success:
                        promise.succeed(result: .created)
                    case .error(let error):
                        promise.fail(error: error)
                    }
                })
            case .error(let error):
                promise.fail(error: error)
            }
        }
        
        return promise.futureResult
    }
}

extension Product {
    
    static func create(with row: TableRow, on connection: DatabaseConnectable) -> Future<Product> {
        let product = Product(name: row.name)
        let future = product.create(on: connection)
        return future
    }
}
