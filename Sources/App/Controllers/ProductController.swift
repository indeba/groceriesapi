//
//  ProductController.swift
//  App
//
//  Created by Hernan G. Gonzalez on 06/10/2018.
//

import Vapor

final class ProductController {
    
    func index(_ req: Request) throws -> Future<[Product]> {
        let query = Product.query(on: req)
        return query.all()
    }
}
