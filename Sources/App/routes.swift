import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    // Ping
    router.get("ping") { req in
        return "pong!"
    }
    
    // Example of configuring a controller
    let products = ProductController()
    router.get("products", use: products.index)
    
    let admin = AdminController()
    router.post("/admin/load", use: admin.load)
//    let todoController = TodoController()
//    router.get("todos", use: todoController.index)
//    router.post("todos", use: todoController.create)
//    router.delete("todos", Todo.parameter, use: todoController.delete)
}
